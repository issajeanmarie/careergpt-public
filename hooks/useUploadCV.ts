import { useUploadResumeMutation } from '@/lib/api/endpoints/Resume/Resume';
import { setResumeResponse } from '@/lib/redux/slices/resumeSlice';
import { uploadCV } from '@/utils/uploadCV';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {}
 */

export const useUploadCV = (file: File | undefined) => {
  const [uploadResume, res] = useUploadResumeMutation();
  const dispatch = useDispatch();

  useEffect(() => {
    if (file) {
      uploadCV({ file, action: uploadResume });
    }
  }, [file]);

  if (res?.data) {
    dispatch(setResumeResponse(res?.data));
  }

  return { ...res };
};
