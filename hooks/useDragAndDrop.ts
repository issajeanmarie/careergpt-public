import { DragEvent, useState } from 'react';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {}
 */

export const useDragAndDrop = () => {
  const [isDragging, setIsDragging] = useState(false);
  const [file, setFile] = useState<File>();

  if (!setFile) return { isDragging: false };

  const handleDragEnter = (e: DragEvent<HTMLElement>) => {
    e.preventDefault();
    setIsDragging(true);
  };

  const handleDragLeave = () => {
    setIsDragging(false);
  };

  const handleDragOver = (e: DragEvent<HTMLElement>) => {
    e.preventDefault();
  };

  const handleDrop = (e: DragEvent<HTMLElement>) => {
    e.preventDefault();
    setIsDragging(false);

    const files = e.dataTransfer.files;
    setFile(files[0]);
  };

  return {
    isDragging,
    setFile,
    file,
    handleDragEnter,
    handleDragLeave,
    handleDrop,
    handleDragOver
  };
};
