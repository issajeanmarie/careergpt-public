import IndexPage from '@/components/Index';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const Home = (): JSX.Element => <IndexPage />;

export default Home;
