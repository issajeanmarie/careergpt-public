import SignUp from '@/components/Auth/SignUp';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const SignUpPage = (): JSX.Element => <SignUp />;

export default SignUpPage;
