import Login from '@/components/Auth/Login';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const LoginPage = (): JSX.Element => <Login />;

export default LoginPage;
