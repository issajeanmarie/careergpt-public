/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

import HomePage from '@/components/Home';

const Dashboard = (): JSX.Element => <HomePage />;

export default Dashboard;
