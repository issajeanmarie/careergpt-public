'use client';
import G_Layout from '@/components/shared/Layout';
import useRouterEvents from '@/hooks/useRouterEvents';
import { store } from '@/lib/redux/store';
import { antdGlobalTheme } from '@/lib/constants';
import '@/styles/globals.css';
import { ConfigProvider } from 'antd';
import type { AppProps } from 'next/app';
import { Provider } from 'react-redux';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

export default function App({ Component, pageProps }: AppProps): JSX.Element {
  useRouterEvents();

  return (
    <ConfigProvider theme={antdGlobalTheme}>
      <Provider {...{ store }}>
        <G_Layout>
          <Component {...pageProps} />
        </G_Layout>
      </Provider>
    </ConfigProvider>
  );
}
