import ForgotPassword from '@/components/Auth/ForgotPassword';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const ForgotPasswordPage = (): JSX.Element => <ForgotPassword />;

export default ForgotPasswordPage;
