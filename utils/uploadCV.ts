import { UploadCVFunctionProps } from '@/lib/types/resume';
import { handleAPIRequests } from './handleAPIRequests';

export const uploadCV = ({ action, file }: UploadCVFunctionProps) => {
  if (file) {
    const formData = new FormData();
    formData.append('file', file);

    handleAPIRequests({
      request: action,
      formData
    });
  }
};
