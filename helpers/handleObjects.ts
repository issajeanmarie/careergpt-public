/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 */

type ObjectSchemeTypes = { [key: string]: string };

class ExtractFromEnums {
  private obj: ObjectSchemeTypes;

  constructor(obj: ObjectSchemeTypes) {
    this.obj = obj;
  }

  formObjFromEnum() {
    const formattedObj: ObjectSchemeTypes = {};

    for (const key in this.obj) {
      if (Object.prototype.hasOwnProperty.call(this.obj, key)) {
        formattedObj[key] = this.obj[key];
      }
    }

    return formattedObj;
  }

  extractKeysFromEnum() {
    const result: string[] = [];

    for (const key in this.obj) {
      if (Object.prototype.hasOwnProperty.call(this.obj, key)) {
        result.push(key);
      }
    }

    return result;
  }
}

export const formObjFromEnum = (obj: ObjectSchemeTypes) => {
  const objFromEnum = new ExtractFromEnums(obj).formObjFromEnum();

  return objFromEnum;
};

export const keysFromEnum = (obj: ObjectSchemeTypes) => {
  const keysFromEnum = new ExtractFromEnums(obj).extractKeysFromEnum();

  return keysFromEnum;
};
