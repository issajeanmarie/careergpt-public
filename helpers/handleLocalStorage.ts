import { LS_MENU_FOLDED } from '@/lib/constants';

/**
 * @since August 2023
 * @author Christophe K. Kwizera <christophekwizera@gmail.com>
 * @see {@link https://kabundege.rw} - Author's website
 * @returns {JSX.Element}
 */
type Types = {
  name: string;
  value: any;
};

type SetMenuFoldTypes = {
  menuFold?: boolean;
};

export const setMenuFold = ({ menuFold }: SetMenuFoldTypes) =>
  localStorage.setItem(LS_MENU_FOLDED, JSON.stringify(menuFold));

export const getMenuFold = () => {
  const status = localStorage.getItem(LS_MENU_FOLDED);
  return status && status !== undefined ? JSON.parse(status) : null;
};

export const saveToLocal = ({ name, value }: Types) => {
  try {
    localStorage.setItem(name, JSON.stringify(value));
  } catch (err) {
    return "Can't save to local storage";
  }
};

export const getFromLocal = (name: string) => {
  try {
    const value = localStorage.getItem(name);

    return value ? JSON.parse(value) : null;
  } catch (error) {
    return null;
  }
};

export const removeFromLocal = (name: string) => {
  localStorage.removeItem(name);
};
