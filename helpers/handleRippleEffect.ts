export const handleRippleEffect = () => {
  const buttons = document.querySelectorAll('button');

  buttons?.forEach((button) => {
    button.addEventListener('click', function (e) {
      const ripple = document.createElement('div');
      const rect = button.getBoundingClientRect();
      const size = Math.max(rect.width, rect.height);
      const x = e.clientX - rect.left - size / 4;
      const y = e.clientY - rect.top - size / 4;

      ripple.style.width = ripple.style.height = `${size}px`;
      ripple.style.left = `${x}px`;
      ripple.style.top = `${y}px`;

      ripple.classList.add('ripple-effect');

      button.appendChild(ripple);

      setTimeout(() => {
        ripple.remove();
      }, 600);
    });
  });
};
