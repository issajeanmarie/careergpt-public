import { COLOR_VALUES } from '@/lib/types/shared/enums';
import { ThemeConfig } from 'antd';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {}
 */

export const routes = {
  home: {
    label: 'Home',
    url: '.'
  },
  signUp: {
    label: 'Sign up',
    url: '/signup'
  },
  login: {
    label: 'Login',
    url: '/login'
  },
  forgotPassword: {
    label: 'ForgotPassword',
    url: '/forgot-password'
  },
  dashboard: {
    label: 'Dashboard',
    url: '/dashboard'
  }
};

export const SIZE_NUMBER_ARRAY = [
  0, 2, 4, 6, 8, 12, 14, 16, 18, 20, 22, 24, 32, 64
] as const;

export const TEXT_DIMENSION_ARRAY = [
  'caption',
  'small',
  'normal',
  'medium',
  'large',
  'extra-large'
] as const;

export const BUTTON_CATEGORIES_ARRAY = ['iconic', 'normal'] as const;

export const SPACING_ARRAY = ['sm', 'md', 'lg', 'xl', 'xxl'] as const;

export const antdGlobalTheme: ThemeConfig = {
  token: {
    borderRadius: 4,
    fontFamily: "'Roboto Mono', 'Franklin Gothic Medium', 'Arial Narrow', Arial"
  },
  components: {
    Layout: {
      colorBgHeader: COLOR_VALUES['cp_white'],
      colorBgTrigger: COLOR_VALUES['cp_white']
    },
    Input: {
      colorBgLayout: COLOR_VALUES['cp_input_bg'],
      colorBorder: COLOR_VALUES['cp_transparent'],
      linkFocusDecoration: 'none',
      colorBgContainer: COLOR_VALUES['cp_transparent'],
      colorPrimary: COLOR_VALUES['cp_primary'],
      colorTextPlaceholder: COLOR_VALUES['cp_grey']
    }
  }
};

export const tagTypes = ['Auth', 'Resume'];

export const CAREER_TKN_KEY = '_careerGPT_tkn_';

export const BASE_API_URL = process.env.NEXT_PUBLIC_API_HOST;

export const LS_MENU_FOLDED = 'LS_MENU_FOLDED';

export const DUMP_RESUME =
  '1234 North 55 Street Bellevue, Nebraska 68005 (402) 292-2345 imasample1@xxx.com SUMMARY OF QUALIFICATIONS Exceptionally well organized and resourceful Professional with more than six years experience and a solid academic background in accounting and financial management; excellent analytical and problem solving skills; able to handle multiple projects while producing high quality work in a fast-paced, deadline-oriented environment. EDUCATION Bachelor of Science, Bellevue University, Bellevue, NE (In Progress) Major: Accounting Minor: Computer Information Systems Expected Graduation Date: January, 20xx GPA to date: 3.95/4.00 PROFESSIONAL ACCOMPLISHMENTS Accounting and Financial Management Developed and maintained accounting records for up to fifty bank accounts. Formulated monthly and year-end financial statements and generated various payroll records, including federal and state payroll reports, annual tax reports, W-2 and 1099 forms, etc. Tested accuracy of account balances and prepared supporting documentation for submission during a comprehensive three-year audit of financial operations. Formulated intricate pro-forma budgets. Calculated and implemented depreciation/amortization schedules. Information Systems Analysis and Problem Solving Converted manual to computerized accounting systems for two organizations. Analyzed and successfully reprogrammed software to meet customer requirements. Researched and corrected problems to assure effective operation of newly computerized systems. WORK HISTORY Student Intern, Financial Accounting Development Program, Mutual of Omaha, Omaha, NE (Summer 20xx)  Accounting Coordinator, Nebraska Special Olympics, Omaha, NE (20xx-20xx) Bookkeeper, SMC, Inc., Omaha, NE (20xx – 20xx) Bookkeeper, First United Methodist Church, Altus, OK (20xx – 20xx) PROFESSIONAL AFFILIATION Member, IMA, Bellevue University Student Chapter COMPUTER SKILLS  Proficient in MS Office (Word, Excel, PowerPoint, Outlook), QuickBooks  Basic Knowledge of MS Access, SQL, Visual Basic, C++ ';
