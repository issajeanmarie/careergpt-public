export type ApiResponseMetadata<T> = T;

export type GenericResponse = ApiResponseMetadata<void>;
