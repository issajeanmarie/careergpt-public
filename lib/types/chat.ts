/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {}
 */

export interface ChatRequest {
  UserChatInput: string;
}

export interface ChatResponse {
  Assistant: string;
}
