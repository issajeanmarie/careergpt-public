import { ReactNode } from 'react';
import { GSetState } from '../shared/reactInherited';

export interface AuthContainerProps {
  children: ReactNode;
  subHeading?: string; // valid on forgot password page, used to hide UIs which are not needed
  formName: string;
  title: string;
}

export interface UploadCVButtonProps {
  showLinkedIn?: boolean;
  pEvents?: boolean;
  setFile: GSetState<File | undefined>;
  loading?: boolean;
}
