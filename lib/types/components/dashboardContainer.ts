import { ReactNode } from 'react';

export interface DashboardContainerProps {
  children: ReactNode;
}
