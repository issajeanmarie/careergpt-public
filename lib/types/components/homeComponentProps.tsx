export interface WidgetProps {
  widget: {
    title: string;
    list: { icon: string; label: string }[];
    button: {
      label: string;
    };
  };
}

export interface RightWidgetProps {
  widget: {
    title: string;
    list: {
      icon: string;
      label: string;
      percentage: string;
      extendedLabel: string;
    }[];
    button: {
      label: string;
    };
  };
}

export interface ChatFormValues {
  message: string;
}

export interface SingleConversation {
  id: number;
  isReplay: boolean;
  message: string;
}

export interface ConversationFromProps {
  conversations: SingleConversation[];
}
