import { ButtonProps } from 'antd';
import { ReactNode } from 'react';
import { COLOR_VALUES } from '../shared/enums';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

export interface GUploadButtonProps extends ButtonProps {
  color?: keyof typeof COLOR_VALUES;
  bg?: keyof typeof COLOR_VALUES;
  fluid?: boolean;
  iconSrc?: string;
  children?: ReactNode;
}
