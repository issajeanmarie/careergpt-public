/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * AUTH TYPES
 * @author Christophe K. Kwizera
 * @authorEmail christophekwizera@awesomity.rw
 * @since AUG 2023
 */

import { MutationTrigger } from '@reduxjs/toolkit/dist/query/react/buildHooks';

export type ResumeResponse = {
  processed_resume: { [key: string]: string[] };
  missing_info: string;
  recommendations: {
    'Potential Jobs': string[];
    'Potential Earnings': string[];
    'Potential Companies': string[];
  };
};

export interface UploadCVFunctionProps {
  file: File;
  action: MutationTrigger<any>;
}
