import {
  COLOR_VALUES,
  SIZE_NUMBER,
  TEXT_DIMENSION,
  TEXT_FONT_WEIGHT
} from './enums';

export interface SimilarTextProps {
  italic?: boolean;
  weight?: keyof typeof TEXT_FONT_WEIGHT;
  underline?: boolean;
  color?: keyof typeof COLOR_VALUES;
  bg?: keyof typeof COLOR_VALUES;
  m?: SIZE_NUMBER;
  p?: SIZE_NUMBER;
  px?: SIZE_NUMBER;
  py?: SIZE_NUMBER;
  pt?: SIZE_NUMBER;
  pb?: SIZE_NUMBER;
  mx?: SIZE_NUMBER;
  my?: SIZE_NUMBER;
  mt?: SIZE_NUMBER;
  mb?: SIZE_NUMBER;
  dimension?: TEXT_DIMENSION;
  pointer?: boolean;
}
