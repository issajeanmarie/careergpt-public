import { ButtonProps, InputProps } from 'antd';
import { TooltipPlacement } from 'antd/es/tooltip';
import { ImageProps } from 'next/image';
import { Dispatch, HTMLProps, SetStateAction } from 'react';
import { BUTTON_CATEGORIES, COLOR_VALUES, SPACING } from './enums';
import { SimilarTextProps } from './similar';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {}
 */

export type GContainerProps = WrapperProps & GContainerExtraProps;

export type GContainerExtraProps = {
  mx?: number;
  my?: number;
  px?: SPACING;
  py?: SPACING;
  mb?: number;
  mt?: number;
  mr?: number;
  ml?: number;
  width?: number;
  height?: number;
  color?: keyof typeof COLOR_VALUES;
  bg?: keyof typeof COLOR_VALUES;
};

export type WrapperProps = HTMLProps<HTMLDivElement>;

export type GParagraphProps = HTMLProps<HTMLParagraphElement> &
  SimilarTextProps & {
    target?: '_blank' | '_parent' | '_self' | '_top';
    uppercase?: boolean;
    pEvents?: boolean;
  };

export type GButtonProps = ButtonProps & SimilarTextProps & GButtonExtraProps;

export type GButtonExtraProps = {
  isDynamic?: boolean;
  faded?: boolean;
  extraFaded?: boolean;
  uppercase?: boolean;
  iconSrc?: string;
  tooltipTitle?: string;
  toolTipPlacement?: TooltipPlacement;
  showTooltip?: boolean;
  iconWidth?: number;
  iconHeight?: number;
  isShort?: boolean;
  category?: BUTTON_CATEGORIES;
  noBorder?: boolean;
  blueEffect?: boolean;
  borderEffect?: boolean;
  iconAlt?: string;
  fluid?: boolean;
};

export type GAnchorProps = React.AnchorHTMLAttributes<HTMLAnchorElement>;

export type GLinkProps = HTMLProps<HTMLAnchorElement> & SimilarTextProps;

export type GStateAction<T> = Dispatch<SetStateAction<T>>;

export type GImageProps = ImageProps & SimilarTextProps & GButtonExtraProps;

export type GInputProps = InputProps & {
  color?: 'cp_white' | 'cp_input_bg';
  prefixIcon?: string;
  suffixIcon?: string;
  isPassword?: boolean;
};

export type GSetState<T> = React.Dispatch<React.SetStateAction<T>>;
