import {
  BUTTON_CATEGORIES_ARRAY,
  SIZE_NUMBER_ARRAY,
  SPACING_ARRAY,
  TEXT_DIMENSION_ARRAY
} from '@/lib/constants';

export enum COLOR_VALUES {
  cp_transparent = 'transparent',
  cp_primary = '#2C5BDC',
  cp_secondary = '#00E5FF',
  cp_dark = '#182036',
  cp_grey = '#8DA2C2',
  cp_light_grey = '#18203614',
  cp_lighter_grey = '#8DA2C21A',
  cp_input_bg = '#8da2c21a',
  cp_extra_light_grey = '#FCFCFC',
  cp_white = '#FFFFFF',
  cp_white_transparent = '#ffffff1a'
}

export type SIZE_NUMBER = (typeof SIZE_NUMBER_ARRAY)[number];

export type BUTTON_CATEGORIES = (typeof BUTTON_CATEGORIES_ARRAY)[number];

export type SPACING = (typeof SPACING_ARRAY)[number];

// Text
export type TEXT_DIMENSION = (typeof TEXT_DIMENSION_ARRAY)[number];

export enum TEXT_FONT_WEIGHT {
  extralight = 'extralight',
  light = 'light',
  normal = 'normal',
  medium = 'medium',
  semibold = 'semibold',
  bold = 'bold',
  extrabold = 'extrabold',
  black = 'black'
}
