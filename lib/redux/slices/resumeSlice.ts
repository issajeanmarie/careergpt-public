import { ResumeResponse } from '@/lib/types/resume';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

type ResumeState = {
  data: ResumeResponse | undefined;
};

const slice = createSlice({
  name: 'resume',
  initialState: {} as ResumeState,
  reducers: {
    setResumeResponse: (state, action: PayloadAction<ResumeResponse>) => {
      state.data = action.payload;
    },
    clearResumeResponse: (state) => {
      state.data = undefined;
    }
  }
});

export const { setResumeResponse, clearResumeResponse } = slice.actions;
export default slice.reducer;
