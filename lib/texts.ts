export const texts = {
  en: {
    indexPage: {
      title: 'Unleash Your Full Potential with',
      name: 'CareerGPT',
      subTitle:
        'Let CareerGPT help you EARN MORE, CONNECT and GET IN TOUCH with potential players in your career.'
    },

    auth: {
      login: 'Login',
      subTitle: 'Provide your email to reset your password',
      forgotPassword: 'Forgot password',
      register: 'Register'
    },

    homePage: {
      rightTitle: 'MY POTENTIAL',
      widgets: {
        jobs: 'POTENTIAL JOBS',
        companies: 'POTENTIAL COMPANIES',
        wage: 'POTENTIAL WAGE',
        skills: 'Skills',
        languages: 'Languages',
        location: 'Location',
        left: {
          but_your_cv: 'But your cv!',
          no_cv_desc:
            ' We need your CV to help you make the right decision for your career!'
        }
      }
    },

    global: {
      or: 'OR',
      coming_soon: 'Coming soon',
      careergpt_working: 'CareerGPT Working...',
      buttons: {
        login: 'Login',
        signup: 'Sign up',
        getStarted: 'GET STARTED',
        uploadCV: 'Upload CV',
        cancel: 'Cancel',
        submit: 'Submit',
        register: 'Register',
        upskill: 'I want to upskill',
        locations: 'Explore different locations',
        earn: 'I want to earn more',
        tell: 'Tell me more',
        connect: 'Connect me',
        save: 'Save'
      },
      inputs: {
        pasteLinkedIn: 'Paste your LinkedIn URL here',
        password: 'Password',
        email: 'Email',
        username: 'Username'
      },
      links: {
        noCV: 'I do not have any of those',
        forgotPwd: 'Forgot Password?'
      },
      others: {
        continueWith: 'Or continue with:',
        footerText: 'Intelligence Group x Awesomity Lab'
      }
    }
  }
};
