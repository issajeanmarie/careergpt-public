import { ChatRequest, ChatResponse } from '@/lib/types/chat';
import { ApiResponseMetadata } from '@/lib/types/shared';
import { baseAPI } from '..';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {}
 */

const chatEndpoints = baseAPI.injectEndpoints({
  endpoints: (builder) => ({
    chat: builder.mutation<ApiResponseMetadata<ChatResponse>, ChatRequest>({
      invalidatesTags: ['chat'],
      query: (DTO) => ({
        url: '/chat_with_user',
        method: 'POST',
        body: DTO
      })
    })
  })
});

export const { useChatMutation } = chatEndpoints;
