import { ResumeResponse } from '@/lib/types/resume';
import { baseAPI } from '../..';
import { ApiResponseMetadata } from '../../../types/shared';
/**
 * AUTH TYPES
 * @author Christophe K. Kwizera
 * @authorEmail christophekwizera@awesomity.rw
 * @since AUG 2023
 */
const settingsApi = baseAPI.injectEndpoints({
  endpoints: (builder) => ({
    /** Personal Information Requests */
    processResume: builder.mutation<
      ApiResponseMetadata<ResumeResponse>,
      string
    >({
      invalidatesTags: ['Resume'],
      query: (DTO) => ({
        url: '/process_resume',
        method: 'POST',
        body: DTO
      })
    }),
    uploadResume: builder.mutation<
      ApiResponseMetadata<ResumeResponse>,
      { formData: FormData }
    >({
      invalidatesTags: ['Resume'],
      query: (DTO) => ({
        url: '/upload',
        method: 'POST',
        body: DTO.formData
      })
    })
  })
});

export const { useProcessResumeMutation, useUploadResumeMutation } =
  settingsApi;
