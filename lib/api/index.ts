import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { BASE_API_URL, CAREER_TKN_KEY, tagTypes } from '../constants';
export const baseAPI = createApi({
  baseQuery: fetchBaseQuery({
    baseUrl: BASE_API_URL,
    prepareHeaders: (headers) => {
      const localToken = localStorage.getItem(CAREER_TKN_KEY);
      if (localToken) headers.set('authorization', `Bearer ${localToken}`);
      return headers;
    }
  }),
  tagTypes,
  endpoints: () => ({})
});
