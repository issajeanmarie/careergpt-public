import { G_Container, G_Div, G_Text } from '@/components/shared';
import useAppDispatch from '@/hooks/useAppDispatch';
import { routes } from '@/lib/constants';
import { setResumeResponse } from '@/lib/redux/slices/resumeSlice';
import { texts } from '@/lib/texts';
import { ResumeResponse } from '@/lib/types/resume';
import { useRouter } from 'next/router';
import { useEffect, useRef } from 'react';
/**
 * @since August 2023
 * @author Christophe K. Kwizera <christophekwizera@gmail.com>
 * @see {@link https://kabundege.rw} - Author's website
 * @returns {JSX.Element}
 */
const LoadingScreen = ({
  isLoading = false,
  data
}: {
  data?: ResumeResponse;
  isLoading: boolean;
}): JSX.Element => {
  const progressBarRef = useRef<HTMLDivElement>(null);
  const wrapperRef = useRef<HTMLDivElement>(null);
  const dispatch = useAppDispatch();
  const router = useRouter();

  useEffect(() => {
    /** animate the content wrapper fo fade-in */
    if (progressBarRef.current) {
      /** start progress bar animation */
      progressBarRef.current.style.width = '98%';
    }
  }, []);

  useEffect(() => {
    if (wrapperRef.current) {
      if (!isLoading) {
        if (data) {
          dispatch(setResumeResponse(data));
          router.replace(routes.dashboard.url);
        }
        wrapperRef.current.classList.add('hidden');
      } else if (isLoading) {
        wrapperRef.current.classList.remove('hidden');
        wrapperRef.current.classList.add('animate-fade-in');
      }
    }
  }, [isLoading, data]);

  return (
    <G_Container
      ref={wrapperRef}
      className="opacity-0 absolute top-0 left-0 w-full z-10"
      style={{ height: 'calc(100vh - 64px)' }}
    >
      <G_Div className="bg-gradient-to-br from-cp_secondary to-cp_primary w-full h-full">
        <G_Div className="w-full h-full prefetch-hero-section" />
      </G_Div>
      <G_Div className="absolute w-full h-full top-0 left-0 bg-cp_primary opacity-30 z-10" />
      <G_Div className="absolute w-2/5 top-1/2 left-[65%] transform -translate-x-1/2 -translate-y-1/2 z-0">
        <G_Text color="cp_extra_light_grey" className="font-light">
          {texts.en.global.careergpt_working}
        </G_Text>
        <G_Div className="relative transform transition-all bg-cp_secondary h-[2px] w-full mt-4 overflow-hidden">
          <G_Div
            ref={progressBarRef}
            className="progress-bar relative h-full w-0 bg-cp_primary"
          />
        </G_Div>
      </G_Div>
    </G_Container>
  );
};

export default LoadingScreen;
