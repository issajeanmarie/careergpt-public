import { antdGlobalTheme } from '@/lib/constants';
import { COLOR_VALUES } from '@/lib/types/shared/enums';
import { GInputProps } from '@/lib/types/shared/reactInherited';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { ConfigProvider, Form, Input, ThemeConfig } from 'antd';
import { FC } from 'react';
import G_Image from '../G_Image/G_Image';

const G_Input: FC<GInputProps> = ({
  className,
  placeholder = 'Placeholder',
  tabIndex = 1,
  isPassword = false,
  color = 'cp_input_bg',
  prefixIcon,
  suffixIcon,
  alt = '',
  name = '',
  ...rest
}) => {
  const buttonTheme: ThemeConfig = {
    ...antdGlobalTheme,
    components: {
      ...antdGlobalTheme.components,
      Input: {
        colorBgContainer: COLOR_VALUES['cp_transparent']
      }
    }
  };

  const inputBgClass = (() => {
    switch (color) {
      case 'cp_white':
        return 'bg-cp_white';
      default:
        return 'bg-cp_input_bg';
    }
  })();

  return (
    <ConfigProvider theme={buttonTheme}>
      {isPassword ? (
        <Form.Item name={name} className="mb-0">
          <Input.Password
            prefix={
              prefixIcon && (
                <G_Image
                  alt={alt}
                  width={16}
                  src={prefixIcon}
                  className="mr-4"
                />
              )
            }
            className={`${className} ${inputBgClass} h-[42px] text-sm px-4`}
            placeholder={placeholder}
            tabIndex={tabIndex}
            size="large"
            name={name}
            {...rest}
            iconRender={(visible) =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
          />
        </Form.Item>
      ) : (
        <Form.Item name={name} className="mb-0">
          <Input
            prefix={
              prefixIcon && (
                <G_Image
                  src={prefixIcon}
                  alt={alt}
                  width={16}
                  className="mr-4"
                />
              )
            }
            suffix={
              suffixIcon && (
                <G_Image
                  src={suffixIcon}
                  alt={alt}
                  width={16}
                  className="ml-4"
                />
              )
            }
            className={`${className} ${inputBgClass} h-[42px] text-sm px-4`}
            placeholder={placeholder}
            size="large"
            tabIndex={tabIndex}
            name={name}
            {...rest}
          />
        </Form.Item>
      )}
    </ConfigProvider>
  );
};

export default G_Input;
