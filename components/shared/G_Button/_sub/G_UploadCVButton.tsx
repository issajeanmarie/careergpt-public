import { texts } from '@/lib/texts';
import { GUploadButtonProps } from '@/lib/types/components/uploadButtonProps';
import { FC } from 'react';
import G_Div from '../../G_Div/G_Div';
import G_Button from '../G_Button';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const G_UploadCVButton: FC<GUploadButtonProps> = ({
  color,
  bg,
  fluid = false,
  iconSrc = '/icons/upload.svg',
  children,
  onClick = () => null,
  loading,
  ...rest
}): JSX.Element => {
  return (
    <G_Div
      className="bg-white flex items-center rounded border hover:border-cp_primary "
      style={{ width: fluid ? '100%' : 'fit-content' }}
      tabIndex={1}
    >
      <G_Div className="border-r">
        <G_Button
          bg={bg || 'cp_transparent'}
          color="cp_dark"
          isDynamic
          noBorder
          blueEffect={false}
          iconSrc={iconSrc}
          onClick={onClick}
          {...rest}
          loading={loading}
        />
      </G_Div>

      <G_Button
        type="default"
        bg={bg || 'cp_transparent'}
        color={color || 'cp_grey'}
        fluid={fluid}
        blueEffect={false}
        onClick={onClick}
        noBorder
        {...rest}
        loading={false}
        disabled={loading as boolean}
        className="border"
      >
        {children || texts?.en?.global?.buttons?.uploadCV}
      </G_Button>
    </G_Div>
  );
};

export default G_UploadCVButton;
