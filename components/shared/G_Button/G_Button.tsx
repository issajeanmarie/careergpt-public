import { handleRippleEffect } from '@/helpers/handleRippleEffect';
import useDidMount from '@/hooks/useDidMount';
import { antdGlobalTheme } from '@/lib/constants';
import { COLOR_VALUES } from '@/lib/types/shared/enums';
import { GButtonProps } from '@/lib/types/shared/reactInherited';
import { Button, ConfigProvider, Tooltip } from 'antd';
import Image from 'next/image';
import { CSSProperties, FC } from 'react';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const G_Button: FC<GButtonProps> = ({
  faded = false,
  extraFaded = false,
  type = 'primary',
  isDynamic = false,
  bg,
  color,
  uppercase = false,
  children,
  iconSrc,
  category,
  size = category === 'iconic' ? 'middle' : 'large',
  tooltipTitle,
  showTooltip = false,
  isShort = false,
  toolTipPlacement,
  iconWidth = 14,
  iconHeight = 14,
  iconAlt = '',
  noBorder,
  className,
  blueEffect = true,
  borderEffect = false,
  tabIndex = 1,
  href,
  mb,
  mt,
  style,
  fluid,
  ...rest
}): JSX.Element => {
  const [didMount] = useDidMount();

  const isIconic = category === 'iconic';

  const styles: CSSProperties = {
    fontSize: '12px',
    minWidth: isDynamic || category == 'iconic' ? '' : isShort ? '0' : '32px',
    background: bg ? COLOR_VALUES[bg] : COLOR_VALUES['cp_primary'],
    color:
      (faded || extraFaded) && !color
        ? COLOR_VALUES['cp_primary']
        : color
        ? COLOR_VALUES[color]
        : COLOR_VALUES['cp_white'],
    padding: !isDynamic && !isIconic ? '0 20px' : '',
    border: noBorder ? 'none' : '',
    marginBottom: `${mb}px`,
    marginTop: `${mt}px`,
    width: `${fluid && '100%'}`,
    ...style
  };

  const classes = `${href ? 'flex items-center justify-center' : ''} ${
    uppercase ? 'uppercase' : ''
  } font-medium ${iconSrc ? 'flex items-center justify-center gap-1' : ''}  ${
    blueEffect && 'blueEffect'
  } btn_customs ${borderEffect && 'hover:border-cp_primary'} ${className} ${
    href && 'flex items-center'
  }`;

  didMount && handleRippleEffect();

  return (
    <ConfigProvider theme={antdGlobalTheme}>
      <Tooltip
        title={tooltipTitle || (showTooltip && children)}
        placement={toolTipPlacement}
      >
        <Button
          href={href}
          icon={
            iconSrc && (
              <Image
                src={iconSrc}
                width={iconWidth}
                height={iconHeight}
                alt={iconAlt}
                className="m-auto"
              />
            )
          }
          type={type || 'primary'}
          style={styles}
          size={size}
          className={classes}
          tabIndex={tabIndex}
          {...rest}
        >
          {children}
        </Button>
      </Tooltip>
    </ConfigProvider>
  );
};

export default G_Button;
