import G_Div from '../G_Div/G_Div';
import G_Image from '../G_Image/G_Image';
import G_Text from '../G_Text/G_Text';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const EmptyData = (): JSX.Element => (
  <G_Div className="grid place-content-center p-12 border text-center rounded">
    <G_Image
      src="/icons/empty.svg"
      className="mx-auto"
      width={64}
      alt="No data icon"
    />

    <G_Text weight="bold" dimension="medium" mb={12} mt={24}>
      Nothing here yet!
    </G_Text>

    <G_Text color="cp_grey" dimension="small">
      Upload your CV to see your potentials
    </G_Text>
  </G_Div>
);

export default EmptyData;
