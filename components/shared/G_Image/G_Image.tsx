import Image from 'next/image';
import { FC } from 'react';
import { GImageProps } from '../../../lib/types/shared/reactInherited';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const G_Image: FC<GImageProps> = ({
  alt = '',
  width = 100,
  height = 100,
  src = '',
  fill = false,
  className
}): JSX.Element => {
  return (
    <Image
      src={src}
      width={width}
      height={height}
      alt={alt}
      fill={fill}
      className={className}
    />
  );
};

export default G_Image;
