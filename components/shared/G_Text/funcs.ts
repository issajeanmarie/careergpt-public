import { TEXT_DIMENSION } from '@/lib/types/shared/enums';

export const dimensionSize = (dimension: TEXT_DIMENSION | undefined) =>
  (() => {
    switch (dimension) {
      case 'extra-large':
        return 'text-[42px] lg:text-[56px]';
      case 'large':
        return 'text-[24px]';
      case 'medium':
        return 'text-[16px]';
      case 'normal':
        return 'text-[14px]';
      case 'caption':
        return 'text-[12px]';
      case 'small':
        return 'text-[10px]';
      default:
        return 'text-[14px]';
    }
  })();
