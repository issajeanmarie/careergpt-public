import { COLOR_VALUES } from '@/lib/types/shared/enums';
import { GParagraphProps } from '@/lib/types/shared/reactInherited';
import Link from 'next/link';
import { CSSProperties, FC } from 'react';
import { dimensionSize } from './funcs';

const G_Text: FC<GParagraphProps> = ({
  children,
  italic,
  underline,
  weight,
  p,
  px,
  py,
  m,
  mx,
  my,
  mt,
  mb,
  pt,
  pb,
  color,
  className,
  bg,
  dimension,
  pointer,
  href,
  target = '_parent',
  pEvents,
  tabIndex = 1,
  style,
  ...rest
}): JSX.Element => {
  const fontSize = dimensionSize(dimension);

  const classes = `${fontSize} font-${weight} ${italic && 'italic'} ${
    underline && 'underline'
  } mx-${mx} my-${my} px-${px} py-${py} mt-${mt} mb-${mb} pt-${pt} pb-${pb} ${className} ${
    pointer ? 'cursor-pointer' : 'cursor-default'
  }`;

  const styles: CSSProperties = {
    color: COLOR_VALUES[color || 'cp_dark'],
    background: bg ? COLOR_VALUES[bg] : 'none',
    padding: `${p || 0}px`,
    margin: `${m || 0}px`,
    marginBottom: `${mb || 0}px`,
    marginTop: `${mt || 0}px`,
    fontWeight: `${
      weight === 'semibold' ? 700 : weight === 'black' ? 900 : weight
    }`,
    pointerEvents: pEvents ? 'none' : 'auto',
    ...style
  };

  return href ? (
    <Link
      style={{ pointerEvents: pEvents ? 'none' : 'auto' }}
      href={href}
      target={target}
    >
      <p
        style={styles}
        className={`${classes} cursor-pointer hover:underline`}
        {...rest}
        tabIndex={tabIndex}
      >
        {children}
      </p>
    </Link>
  ) : (
    <p style={styles} className={`${classes}`} {...rest}>
      {children}
    </p>
  );
};

export default G_Text;
