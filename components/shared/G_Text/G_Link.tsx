import Link from 'next/link';
import { FC } from 'react';
import { COLOR_VALUES } from '../../../lib/types/shared/enums';
import { GLinkProps } from '../../../lib/types/shared/reactInherited';
import { dimensionSize } from './funcs';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const G_Link: FC<GLinkProps> = ({
  children,
  italic,
  underline,
  weight,
  p,
  px,
  py,
  m,
  mx,
  my,
  mt,
  mb,
  pt,
  pb,
  color,
  className,
  bg,
  dimension,
  pointer = true,
  href,
  tabIndex = 1,
  ...rest
}): JSX.Element => {
  const fontSize = dimensionSize(dimension);

  return (
    <Link href={href || ''}>
      <span
        style={{
          color: COLOR_VALUES[color || 'cp_dark'],
          background: bg ? COLOR_VALUES[bg] : 'none',
          padding: `${p || 0}px`,
          margin: `${m || 0}px`
        }}
        className={`${fontSize} font-${weight} ${italic && 'italic'} ${
          underline && 'underline'
        } mx-${mx} my-${my} px-${px} py-${py} mt-${mt} mb-${mb} pt-${pt} pb-${pb} ${className} ${
          pointer ? 'cursor-pointer' : 'cursor-default'
        }`}
        tabIndex={tabIndex}
        {...rest}
      >
        {children}
      </span>
    </Link>
  );
};

export default G_Link;
