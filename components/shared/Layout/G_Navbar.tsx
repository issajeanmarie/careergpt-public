import { routes } from '@/lib/constants';
import { texts } from '@/lib/texts';
import { NavbarProps } from '@/lib/types/layout';
import { Layout } from 'antd';
import { FC } from 'react';
import G_Button from '../G_Button/G_Button';
import G_Div from '../G_Div/G_Div';
import G_Image from '../G_Image/G_Image';
import G_Link from '../G_Text/G_Link';
import G_Container from './_sub/G_Container';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const { Header } = Layout;

const G_Navbar: FC<NavbarProps> = ({ atAuth = false }): JSX.Element => {
  return (
    <Header
      className={`flex items-center ${!atAuth && 'shadow-md'} sticky top-0 ${
        atAuth && 'bg-cp_transparent'
      } relative z-20`}
      style={{ padding: 0, height: 'fit-content' }}
    >
      <G_Container
        className="w-full h-[64px] flex items-center justify-between"
        px="xxl"
      >
        <G_Link href={routes.home.url} tabIndex={1}>
          <G_Image
            src={atAuth ? '/icons/logo-white.png' : '/icons/logo.png'}
            alt="CareerGPT Logo"
            width={120}
          />
        </G_Link>

        {atAuth ? (
          <G_Link href={routes.home.url} tabIndex={1}>
            <G_Image src="./icons/close.svg" alt="CareerGPT Logo" width={16} />
          </G_Link>
        ) : (
          <G_Div className="flex items-center gap-3">
            <G_Button
              href={routes.login.url}
              bg="cp_light_grey"
              color="cp_dark"
              blueEffect={false}
              borderEffect
            >
              Login
            </G_Button>

            <G_Button href={routes.signUp.url}>
              {texts?.en?.global?.buttons?.signup}
            </G_Button>
          </G_Div>
        )}
      </G_Container>
    </Header>
  );
};

export default G_Navbar;
