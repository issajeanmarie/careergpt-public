import { Layout } from 'antd';
import { FC, ReactNode } from 'react';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const { Content } = Layout;

const G_Content: FC<{ children: ReactNode }> = ({ children }): JSX.Element => (
  <Content
    className="lg:flex lg:items-center py-16 lg:py-0"
    style={{
      height: 'calc(100vh - 64px)',
      overflowY: 'auto'
    }}
  >
    {children}
  </Content>
);

export default G_Content;
