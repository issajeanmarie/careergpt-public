import { GContainerProps } from '@/lib/types/shared/reactInherited';
import { CSSProperties, forwardRef } from 'react';
import G_Div from '../../G_Div/G_Div';
import { spacingValues } from '../funcs';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const G_Container = forwardRef<HTMLDivElement, GContainerProps>(
  ({ children, className, px, py, style, ...rest }, ref): JSX.Element => {
    const pxSize = px ? spacingValues(px) : 0;
    const pySize = py ? spacingValues(py) : 0;

    const styles: CSSProperties = {
      paddingLeft: `${pxSize}px`,
      paddingRight: `${pxSize}px`,
      paddingTop: `${pySize}px`,
      paddingBottom: `${pySize}px`,
      ...style
    };

    return (
      <G_Div
        style={styles}
        className={`custom-container relative ${className}`}
        {...{ ...rest, ref }}
      >
        {children}
      </G_Div>
    );
  }
);

G_Container.displayName = 'G_Container';

export default G_Container;
