import { FC, ReactNode } from 'react';
import G_Div from '../../G_Div/G_Div';
import G_Navbar from '../G_Navbar';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const G_AuthLayout: FC<{ children: ReactNode }> = ({
  children
}): JSX.Element => {
  return (
    <G_Div className="bg-cp_primary h-screen w-screen auth-screen">
      <G_Navbar atAuth />

      <G_Div
        className="lg:flex lg:items-center lg:justify-center w-full py-6 lg:py-0"
        style={{
          height: 'calc(100vh - 64px)',
          overflowY: 'auto'
        }}
      >
        {children}
      </G_Div>
    </G_Div>
  );
};

export default G_AuthLayout;
