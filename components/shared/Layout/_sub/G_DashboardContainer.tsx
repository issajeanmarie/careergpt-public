import { DashboardContainerProps } from '@/lib/types/components/dashboardContainer';
import { FC } from 'react';
import G_Div from '../../G_Div/G_Div';
import G_Container from './G_Container';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const G_DashboardContainer: FC<DashboardContainerProps> = ({
  children
}): JSX.Element => (
  <G_Div
    className="bg-cp_extra_light_grey h-screen"
    style={{
      height: 'calc(100vh - 64px)'
    }}
  >
    <G_Container
      px="xxl"
      className="auth-screen h-[400px] w-full relative bg-cp_primary"
    >
      <G_Div
        className="bg-cp_transparent h-[2000px]"
        style={{
          height: 'calc(100vh - 64px)',
          overflowY: 'hidden'
        }}
      >
        {children}
      </G_Div>
    </G_Container>
  </G_Div>
);

export default G_DashboardContainer;
