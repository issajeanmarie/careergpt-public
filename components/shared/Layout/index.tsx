import { routes } from '@/lib/constants';
import { COLOR_VALUES } from '@/lib/types/shared/enums';
import { Layout } from 'antd';
import { useRouter } from 'next/router';
import { FC, ReactNode } from 'react';
import G_Navbar from './G_Navbar';
import G_AuthLayout from './_sub/G_AuthLayout';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const G_Layout: FC<{ children: ReactNode }> = ({ children }): JSX.Element => {
  const router = useRouter();
  const isAt = {
    forgotPassword: router.pathname === routes.forgotPassword.url,
    signUp: router.pathname === routes.signUp.url,
    login: router.pathname === routes.login.url
  };

  return isAt?.signUp || isAt?.login || isAt.forgotPassword ? (
    <G_AuthLayout>{children}</G_AuthLayout>
  ) : (
    <Layout style={{ background: COLOR_VALUES['cp_white'] }}>
      <G_Navbar />
      {children}
    </Layout>
  );
};

export default G_Layout;
