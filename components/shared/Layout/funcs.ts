import { SPACING } from '@/lib/types/shared/enums';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {number}
 * This function accepts spacing enums and returns an actual number
 */

export const spacingValues = (space: SPACING): number =>
  (() => {
    switch (space) {
      case 'sm':
        return 24;
        break;
      case 'md':
        return 12;
        break;
      case 'lg':
        return 12;
        break;
      case 'xl':
        return 12;
        break;
      case 'xxl':
        return 120;
        break;
      default:
        return 0;
        break;
    }
  })();
