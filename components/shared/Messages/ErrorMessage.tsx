import { notification } from 'antd';

export const ErrorMessage = (
  text: string | undefined,
  type: 'error' | 'info' = 'error'
) => {
  return (
    <>
      {notification[type]({
        message: `${type?.charAt(0)?.toUpperCase()}${type.slice(1)}`,
        description: text
          ? text
          : 'Something is wrong. Please report this error',
        placement: 'bottomRight',
        duration: 10
      })}
    </>
  );
};
