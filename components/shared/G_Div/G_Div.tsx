import { forwardRef } from 'react';
import { WrapperProps } from '../../../lib/types/shared/reactInherited';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const G_Div = forwardRef<HTMLDivElement, WrapperProps>(
  ({ children, ...rest }, ref): JSX.Element => {
    return <div {...{ ...rest, ref }}>{children}</div>;
  }
);

G_Div.displayName = 'G_Div';

export default G_Div;
