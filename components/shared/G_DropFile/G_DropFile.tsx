import G_Div from '../G_Div/G_Div';
import G_Image from '../G_Image/G_Image';
import G_Text from '../G_Text/G_Text';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const G_DropFile = (): JSX.Element => (
  <G_Div
    className="absolute flex flex-col gap-12 items-center justify-center bg-[rgba(255,255,255,0.95)] z-10 top-0 left-0 h-full w-full"
    style={{ pointerEvents: 'none' }}
  >
    <G_Image src="/icons/upload.svg" alt="Drop file to upload" />

    <G_Text pEvents dimension="normal" weight="semibold" color="cp_grey">
      Drop your file to upload!
    </G_Text>
  </G_Div>
);

export default G_DropFile;
