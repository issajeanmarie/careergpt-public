import { routes } from '@/lib/constants';
import { texts } from '@/lib/texts';
import { G_Button, G_Input } from '../shared';
import AuthContainer from './_sub/AuthContainer';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const ForgotPassword = (): JSX.Element => {
  return (
    <AuthContainer
      formName="login"
      title={texts?.en?.auth?.forgotPassword}
      subHeading={texts?.en?.auth?.subTitle}
    >
      <G_Input
        color="cp_white"
        placeholder="Email"
        prefixIcon="./icons/email.svg"
      />
      <G_Button uppercase mb={32} mt={6}>
        {texts?.en?.global?.buttons?.submit}
      </G_Button>
      <G_Button
        uppercase
        bg="cp_white_transparent"
        mb={24}
        href={routes.login.url}
      >
        {texts?.en?.global?.buttons?.cancel}
      </G_Button>
    </AuthContainer>
  );
};

export default ForgotPassword;
