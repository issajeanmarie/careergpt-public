import { routes } from '@/lib/constants';
import { texts } from '@/lib/texts';
import { G_Button, G_Input, G_Text } from '../shared';
import AuthContainer from './_sub/AuthContainer';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const Login = (): JSX.Element => {
  return (
    <AuthContainer title={texts?.en?.auth?.login} formName="login">
      <>
        <G_Input
          color="cp_white"
          prefixIcon="./icons/email.svg"
          placeholder={texts?.en?.global?.inputs?.email}
        />

        <G_Input
          color="cp_white"
          prefixIcon="./icons/lock-sm.svg"
          placeholder={texts?.en?.global?.inputs?.password}
        />

        <G_Button uppercase mb={32} mt={6}>
          {texts?.en?.global?.buttons?.login}
        </G_Button>

        <G_Text
          href={routes.forgotPassword.url}
          className="text-center underline"
          color="cp_white"
          mb={32}
        >
          {texts?.en?.global?.links?.forgotPwd}
        </G_Text>

        <G_Button
          uppercase
          bg="cp_white_transparent"
          href={routes.signUp.url}
          mb={24}
        >
          {texts?.en?.global?.buttons?.signup}
        </G_Button>
      </>
    </AuthContainer>
  );
};

export default Login;
