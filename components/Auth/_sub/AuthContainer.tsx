import { G_Button, G_Container, G_Text } from '@/components/shared';
import { texts } from '@/lib/texts';
import { AuthContainerProps } from '@/lib/types/components/autht';
import { Col, Form, Row } from 'antd';
import { FC } from 'react';
import { socialIcons } from '../consts';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const AuthContainer: FC<AuthContainerProps> = ({
  subHeading, // valid on forgot password page, used to hide UIs which are not needed
  children,
  formName,
  title
}): JSX.Element => {
  return (
    <G_Container className="flex items-center flex-col w-full sm:w-[300px]">
      <G_Text
        dimension="large"
        color="cp_white"
        weight="bold"
        mb={subHeading ? 32 : 64}
      >
        {title}
      </G_Text>

      {!!subHeading && (
        <G_Text
          className="text-center"
          weight="extralight"
          color="cp_white"
          mb={64}
        >
          {subHeading}
        </G_Text>
      )}

      <Form name={formName} className="flex flex-col gap-2 w-full">
        {children}

        {!subHeading && (
          <>
            <G_Text
              className="text-center"
              color="cp_white"
              mb={24}
              weight="extralight"
            >
              {texts?.en?.global?.others?.continueWith}
            </G_Text>

            <Row gutter={32} justify="center">
              {socialIcons.map((icon) => (
                <Col key={icon.id}>
                  <G_Button
                    iconSrc={icon.iconSrc}
                    isDynamic
                    bg="cp_white_transparent"
                    iconAlt={icon.iconAlt}
                  />
                </Col>
              ))}
            </Row>
          </>
        )}
      </Form>

      <G_Text
        className="text-center opacity-50 !mt-[140px]"
        color="cp_white"
        weight="extralight"
      >
        {texts?.en?.global?.others?.footerText}
      </G_Text>
    </G_Container>
  );
};

export default AuthContainer;
