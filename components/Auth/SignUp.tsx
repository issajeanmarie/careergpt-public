import { routes } from '@/lib/constants';
import { texts } from '@/lib/texts';
import { G_Button, G_Input } from '../shared';
import AuthContainer from './_sub/AuthContainer';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const SignUp = (): JSX.Element => {
  return (
    <AuthContainer title={texts?.en?.auth?.register} formName="signup">
      <>
        <G_Input
          color="cp_white"
          prefixIcon="./icons/person.svg"
          placeholder="Username"
        />

        <G_Input
          color="cp_white"
          prefixIcon="./icons/email.svg"
          placeholder={texts?.en?.global?.inputs?.email}
        />

        <G_Input
          color="cp_white"
          prefixIcon="./icons/lock-sm.svg"
          placeholder={texts?.en?.global?.inputs?.password}
        />

        <G_Button uppercase mb={32} mt={6}>
          {texts?.en?.global?.buttons?.register}
        </G_Button>

        <G_Button
          uppercase
          bg="cp_white_transparent"
          mb={24}
          href={routes.login.url}
        >
          {texts?.en?.global?.buttons?.login}
        </G_Button>
      </>
    </AuthContainer>
  );
};

export default SignUp;
