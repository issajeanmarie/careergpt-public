export const socialIcons = [
  {
    id: 1,
    iconAlt: 'Google icon',
    iconSrc: './icons/google.svg'
  },
  {
    id: 2,
    iconAlt: 'Twitter icon',
    iconSrc: './icons/twitter.svg'
  },
  {
    id: 3,
    iconAlt: 'Facebook icon',
    iconSrc: './icons/facebook.svg'
  }
];

export const GPT_DEFAULT_MESSAGE =
  'Hey, I am CareerGPT, your career assistant. Please upload your CV for me to understand you better!';
