import { useDragAndDrop } from '@/hooks/useDragAndDrop';
import { useUploadCV } from '@/hooks/useUploadCV';
import { texts } from '@/lib/texts';
import LoadingScreen from '../Loading';
import { G_Container, G_Div, G_Text } from '../shared';
import G_DropFile from '../shared/G_DropFile/G_DropFile';
import G_Content from '../shared/Layout/_sub/G_Content';
import CVInputs from './CVInputs';
import { useRedirectOnCV } from './hooks';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const IndexPage = (): JSX.Element => {
  const {
    isDragging,
    setFile,
    file,
    handleDragEnter,
    handleDragLeave,
    handleDragOver,
    handleDrop
  } = useDragAndDrop();

  const { data, isLoading } = useUploadCV(file);
  useRedirectOnCV(data);

  return isLoading ? (
    <LoadingScreen isLoading={isLoading} data={data} />
  ) : (
    <>
      <G_Content>
        <G_Div className="w-1/2 h-full home-hero-section" />

        <G_Container
          px="xxl"
          onDragEnter={handleDragEnter}
          onDragLeave={handleDragLeave}
          onDragOver={handleDragOver}
          onDrop={handleDrop}
        >
          {isDragging && <G_DropFile />}

          <G_Text pEvents dimension="extra-large" weight="semibold">
            {texts?.en?.indexPage?.title}
          </G_Text>

          <G_Text
            pEvents
            dimension="extra-large"
            weight="black"
            color="cp_secondary"
            mb={24}
          >
            {texts?.en?.indexPage?.name}
          </G_Text>

          <G_Text
            pEvents
            dimension="normal"
            mb={24}
            color="cp_dark"
            className="w-[100%] xl:w-[60%] font-extralight"
          >
            {texts?.en?.indexPage?.subTitle}
          </G_Text>

          {setFile && (
            <CVInputs pEvents={isDragging} showLinkedIn setFile={setFile} />
          )}

          <G_Text pEvents href="#" underline weight="bold">
            {texts?.en?.global?.links?.noCV}
          </G_Text>
        </G_Container>
      </G_Content>
    </>
  );
};
export default IndexPage;
