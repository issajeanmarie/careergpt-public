import { routes } from '@/lib/constants';
import { ResumeResponse } from '@/lib/types/resume';
import router from 'next/router';
import { useEffect } from 'react';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {}
 */

export const useRedirectOnCV = (data: ResumeResponse | undefined) => {
  useEffect(() => {
    if (data) {
      router.replace(routes.dashboard.url);
    }
  }, [data]);
  return null;
};
