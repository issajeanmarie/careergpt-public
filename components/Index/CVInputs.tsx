import { texts } from '@/lib/texts';
import { UploadCVButtonProps } from '@/lib/types/components/autht';
import { Col, Row } from 'antd';
import { FC, useRef } from 'react';
import { G_Button, G_Input, G_Text } from '../shared';
import G_UploadCVButton from '../shared/G_Button/_sub/G_UploadCVButton';

/**
 * @since July 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const CVInputs: FC<UploadCVButtonProps> = ({
  showLinkedIn = false,
  pEvents,
  loading,
  setFile
}): JSX.Element => {
  const inputRef = useRef<HTMLInputElement>(null);

  return (
    <Row
      justify="space-between"
      align="middle"
      gutter={32}
      style={{ pointerEvents: pEvents ? 'none' : 'auto' }}
      wrap={false}
      className="mb-12 flex-col items-start lg:flex-row lg:items-center"
    >
      <Col
        className="mb-4"
        // pick a document
        onClick={() => {
          if (inputRef.current) inputRef.current.click();
        }}
      >
        <G_UploadCVButton loading={loading} />
      </Col>

      <input
        type="file"
        ref={inputRef}
        className="hidden"
        accept="application/pdf"
        onChange={(ev) => {
          if (ev.target.files) {
            setFile(ev.target.files[0]);
          }
        }}
      />

      {showLinkedIn && (
        <>
          <Col className="mb-4">
            <G_Text weight="bold">{texts?.en?.global?.or}</G_Text>
          </Col>

          <Col flex="auto" className="mb-4">
            <Row
              align="middle"
              gutter={0}
              wrap={false}
              justify="end"
              className="flex-col items-start sm:flex-row gap-4"
            >
              <Col flex="auto">
                <G_Input
                  className="w-[100%]"
                  placeholder={texts?.en?.global?.coming_soon}
                  prefixIcon="/icons/linkedin.svg"
                  disabled
                />
              </Col>

              <Col>
                <G_Button uppercase disabled>
                  {texts?.en?.global?.buttons?.getStarted}
                </G_Button>
              </Col>
            </Row>
          </Col>
        </>
      )}
    </Row>
  );
};

export default CVInputs;
