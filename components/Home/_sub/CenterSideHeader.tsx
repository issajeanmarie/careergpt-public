import { G_Button, G_Div, G_Text } from '@/components/shared';
import { texts } from '@/lib/texts';
import dayjs from 'dayjs';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const CenterSideHeader = (): JSX.Element => {
  const canSave = false;

  return (
    <G_Div className="p-4 pr-1 border-b border-cp_light_grey flex items-center justify-between">
      <G_Text color="cp_grey" weight="normal">
        Convo-{dayjs().format('DD-MM-YYYY')}
      </G_Text>

      {canSave && (
        <G_Button
          isDynamic
          bg="cp_white"
          color="cp_dark"
          iconSrc="./icons/like.svg"
          iconAlt="Save conversation"
          type="default"
          className="border-none"
        >
          {texts?.en?.global?.buttons?.save}
        </G_Button>
      )}
    </G_Div>
  );
};

export default CenterSideHeader;
