import { G_Button, G_Div, G_Image, G_Text } from '@/components/shared';
import { WidgetProps } from '@/lib/types/components/homeComponentProps';
import { FC } from 'react';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const HomeWidget: FC<WidgetProps> = ({ widget }): JSX.Element => (
  <G_Div className="px-10 py-6">
    <G_Text
      color="cp_grey"
      weight="semibold"
      className="line-clamp-3 flex-1"
      mb={32}
      title={widget?.title}
    >
      {widget?.title}
    </G_Text>

    {widget?.list?.map((item) => (
      <G_Div className="flex items-start gap-6 mb-4" key={item.label}>
        <G_Div className="bg-cp_lighter_grey p-3 rounded">
          <G_Image src={item.icon} alt="Icon" width={12} />
        </G_Div>

        <G_Text
          weight="semibold"
          className="line-clamp-3 flex-1"
          title={item.label}
        >
          {item.label}
        </G_Text>
      </G_Div>
    ))}

    {!!widget.button.label && (
      <G_Button
        style={{ width: '100%' }}
        bg="cp_white"
        color="cp_primary"
        blueEffect={false}
        type="default"
        weight="bold"
        mt={24}
      >
        {widget.button.label}
      </G_Button>
    )}
  </G_Div>
);

export default HomeWidget;
