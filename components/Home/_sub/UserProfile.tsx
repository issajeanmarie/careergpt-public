/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

import { G_Div, G_Image, G_Text } from '@/components/shared';

const UserProfile = (): JSX.Element => (
  <G_Div className="flex items-center  gap-6 bg-cp_dark w-[100%] p-2 rounded-t">
    <G_Div className="bg-slate-700 p-10 rounded">
      <G_Image width={24} src="/icons/person.svg" alt="User profile image" />
    </G_Div>

    <G_Div>
      <G_Text color="cp_white" mb={6} dimension="medium">
        Yves Honore
      </G_Text>

      <G_Div className="flex items-center gap-1 mb-1">
        <G_Image
          src="./icons/linkedin-rect.svg"
          width={12}
          alt="Linkedin icon"
        />

        <G_Text color="cp_grey" dimension="caption" weight="extralight">
          /yveshonore14
        </G_Text>
      </G_Div>

      <G_Text color="cp_secondary" dimension="caption" weight="extralight">
        Product designer
      </G_Text>
    </G_Div>
  </G_Div>
);

export default UserProfile;
