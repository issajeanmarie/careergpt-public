import { G_Div, G_Text } from '@/components/shared';
import EmptyData from '@/components/shared/EmptyData/EmptyData';
import useAppSelector from '@/hooks/useAppSelector';
import { texts } from '@/lib/texts';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const HomeRightSideHeader = (): JSX.Element => {
  const { resume } = useAppSelector((state) => state);

  return (
    <>
      <G_Div className="p-10 py-6 bg-cp_lighter_grey">
        <G_Text color="cp_dark" weight="bold">
          {texts?.en?.homePage?.rightTitle}
        </G_Text>
      </G_Div>

      <G_Div className="p-10 py-6 z-0">
        {!resume?.data ? (
          <EmptyData />
        ) : (
          <>
            <G_Text color="cp_grey" weight="semibold">
              {texts?.en?.homePage?.widgets?.wage}
            </G_Text>

            <G_Div className="flex items-end gap-4 truncate mt-4">
              <G_Text
                color="cp_primary"
                dimension="large"
                weight="bold"
                className="truncate"
              >
                ${''}
                {resume?.data
                  ? resume.data?.recommendations?.['Potential Earnings'][0]
                  : 'N/A'}
              </G_Text>

              <G_Text dimension="normal" weight="extralight" mb={4}>
                / Month
              </G_Text>
            </G_Div>

            {/*  Hidden for UI purposes            
            <G_Button
              style={{ width: '100%' }}
              bg="cp_white"
              color="cp_primary"
              blueEffect={false}
              type="default"
              weight="bold"
              mt={24}
            >
              I want to earn more
            </G_Button> */}
          </>
        )}
      </G_Div>
    </>
  );
};

export default HomeRightSideHeader;
