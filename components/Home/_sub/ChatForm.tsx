import { G_Button, G_Div, G_Input } from '@/components/shared';
import { ChatFormValues } from '@/lib/types/components/homeComponentProps';
import { Col, Form, Skeleton } from 'antd';
import { useSendChatRequest } from '../useAPI';
import Conversation from './Conversation';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const ChatForm = (): JSX.Element => {
  const [form] = Form.useForm();

  const { isLoading, onFinish, conversations } = useSendChatRequest(form);

  return (
    <G_Div className="absolute bottom-0 w-full p-4">
      <G_Div
        className="overflow-y-auto"
        style={{ height: 'calc(100vh - 320px)' }}
      >
        <Conversation conversations={conversations} />

        {isLoading && <Skeleton avatar round active />}
      </G_Div>

      <Form<ChatFormValues>
        name="chat-form"
        onFinish={onFinish}
        form={form}
        className="border-t pt-4 border-cp_light_grey w-full flex items-center justify-between gap-4"
      >
        <Col flex="auto">
          <G_Input
            className="w-[100%]"
            placeholder="Type here..."
            color="cp_input_bg"
            name="message"
          />
        </Col>

        <G_Button
          iconWidth={24}
          iconSrc="/icons/send-white.svg"
          htmlType="submit"
          form="chat-form"
          loading={isLoading}
          isDynamic
          className="primary-btn-gradient"
        />
      </Form>
    </G_Div>
  );
};

export default ChatForm;
