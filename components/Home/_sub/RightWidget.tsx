import { G_Button, G_Div, G_Image, G_Link, G_Text } from '@/components/shared';
import { RightWidgetProps } from '@/lib/types/components/homeComponentProps';
import { FC } from 'react';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const HomeRightWidget: FC<RightWidgetProps> = ({ widget }): JSX.Element => {
  return (
    <G_Div className="p-10 py-6">
      <G_Text color="cp_grey" weight="semibold" mb={32}>
        {widget?.title}
      </G_Text>

      {widget?.list?.map((item) => (
        <G_Div className="flex items-center gap-6 mb-4" key={item.label}>
          <G_Div className="bg-cp_lighter_grey p-3 rounded">
            <G_Image src={item.icon} alt="Icon" width={12} />
          </G_Div>

          {item.extendedLabel ? (
            <G_Text weight="semibold" className="truncate">
              {item.label} <span className="text-cp_grey">&</span>{' '}
              <G_Link href="#" color="cp_primary">
                {item.extendedLabel}
              </G_Link>
            </G_Text>
          ) : (
            <G_Text weight="semibold">{item.label}</G_Text>
          )}

          {/* hidden for UI purposes 
            <G_Text
              className="flex-auto text-right"
              color="cp_grey"
              weight="extralight"
            >
              {item.percentage}
            </G_Text> */}
        </G_Div>
      ))}

      {!!widget.button.label && (
        <G_Button
          style={{ width: '100%' }}
          bg="cp_white"
          color="cp_primary"
          blueEffect={false}
          type="default"
          weight="bold"
          mt={24}
        >
          {widget.button.label}
        </G_Button>
      )}
    </G_Div>
  );
};

export default HomeRightWidget;
