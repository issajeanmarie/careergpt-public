import { G_Div, G_Image, G_Text } from '@/components/shared';
import { ConversationFromProps } from '@/lib/types/components/homeComponentProps';
import { FC } from 'react';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const Conversation: FC<ConversationFromProps> = ({
  conversations
}): JSX.Element => {
  return (
    <>
      {conversations?.map((conversation) =>
        conversation.isReplay ? (
          <G_Div className="mb-6 flex items-start gap-6" key={conversation.id}>
            <G_Div className="w-[42px] h-[42px] flex-none">
              <G_Image
                height={100}
                width={100}
                src="/icons/gpt-profile.png"
                alt="ChatGPT Profile"
              />
            </G_Div>

            <G_Div className="bg-cp_lighter_grey gpt-message p-4 px-8 rounded flex-auto response-container relative">
              <G_Text
                dimension="caption"
                weight="light"
                className="leading-relaxed"
              >
                {conversation?.message}
              </G_Text>
            </G_Div>
          </G_Div>
        ) : (
          <UserRequestText
            message={conversation.message}
            key={conversation.id}
          />
        )
      )}
    </>
  );
};

export default Conversation;

export const UserRequestText = ({
  message
}: {
  message: string;
}): JSX.Element => (
  <G_Div className="mb-6 flex items-start gap-6">
    <G_Div className="user-message p-4 rounded flex-auto response-container relative">
      <G_Text
        color="cp_white"
        dimension="caption"
        weight="medium"
        className="leading-relaxed"
      >
        {message}
      </G_Text>
    </G_Div>

    <G_Div className="w-[42px] h-[42px] flex-none bg-cp_lighter_grey p-3 rounded">
      <G_Image src="/icons/person-blue.svg" alt="Icon" />
    </G_Div>
  </G_Div>
);
