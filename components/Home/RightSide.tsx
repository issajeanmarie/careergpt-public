import useAppSelector from '@/hooks/useAppSelector';
import { Col } from 'antd';
import React, { useMemo } from 'react';
import { G_Div } from '../shared';
import HomeRightSideHeader from './_sub/HomeRightSideHeader';
import HomeRightWidget from './_sub/RightWidget';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const HomeRightSide = (): JSX.Element => {
  const { resume } = useAppSelector((state) => state);

  const data = useMemo(() => {
    if (!resume.data) return [];

    const potentialJobs = resume?.data?.recommendations?.['Potential Jobs'].map(
      (job) => ({
        icon: './icons/star.svg',
        percentage: '70%',
        extendedLabel: '',
        label: job
      })
    );

    const potentialCompanies = resume?.data?.recommendations?.[
      'Potential Companies'
    ]?.map((company) => ({
      percentage: '',
      label: company,
      icon: './icons/institution.svg',
      extendedLabel: '' //'20 other Companies'
    }));

    return [
      {
        title: 'POTENTIAL JOBS',
        list: potentialJobs,
        button: {
          label: '' //'Tell me more'
        }
      },
      {
        title: 'POTENTIAL COMPANIES',
        list: potentialCompanies,
        button: {
          label: '' //'Connect me'
        }
      }
    ];
  }, [resume.data]);

  return (
    <Col
      span={7}
      className="pb-12 bg-white shadow-md rounded overflow-y-auto"
      style={{ height: 'calc(100vh - 160px)' }}
    >
      <HomeRightSideHeader />

      <G_Div>
        {data
          ? React.Children.toArray(
              data?.map((widget) => (
                <HomeRightWidget widget={widget} key={widget.title} />
              ))
            )
          : null}
      </G_Div>
    </Col>
  );
};

export default HomeRightSide;
