import { Col } from 'antd';
import CenterSideHeader from './_sub/CenterSideHeader';
import ChatForm from './_sub/ChatForm';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const HomeCenterSide = (): JSX.Element => (
  <Col
    flex="auto"
    className="pb-12 bg-white shadow-md rounded relative"
    style={{ height: 'calc(100vh - 160px)' }}
  >
    <CenterSideHeader />

    <ChatForm />
  </Col>
);

export default HomeCenterSide;
