import useAppSelector from '@/hooks/useAppSelector';
import { useDragAndDrop } from '@/hooks/useDragAndDrop';
import { useUploadCV } from '@/hooks/useUploadCV';
import { texts } from '@/lib/texts';
import { Col } from 'antd';
import React, { useMemo } from 'react';
import CVInputs from '../Index/CVInputs';
import { G_Div, G_Text } from '../shared';
import G_DropFile from '../shared/G_DropFile/G_DropFile';
import UserProfile from './_sub/UserProfile';
import HomeWidget from './_sub/Widget';

/**
 * @since August 2023
 * @author Christophe K. Kwizera <christophekwizera@gmail.com>
 * @see {@link https://kabundege.rw} - Author's website
 * @returns {JSX.Element}
 */

const HomeLeftSide = (): JSX.Element => {
  const { resume } = useAppSelector((state) => state);

  const {
    isDragging,
    setFile,
    file,
    handleDragEnter,
    handleDragOver,
    handleDragLeave,
    handleDrop
  } = useDragAndDrop();

  const { isLoading } = useUploadCV(file);

  const structuredData = useMemo(() => {
    if (!resume.data) return [];

    return Object.entries(resume?.data?.processed_resume)
      .filter(([key]) => key.length > 1)
      .map(([key, value]) => ({
        title: key,
        button: {
          label: ''
        },
        list: value.map((label) => ({ icon: './icons/work-alt.svg', label }))
      }));
  }, [resume]);

  return (
    <Col
      span={7}
      className="pb-12 bg-white shadow-md rounded"
      style={{ height: 'calc(100vh - 160px)' }}
    >
      <UserProfile />

      <G_Div className="overflow-y-scroll max-h-full h-full pb-20">
        {structuredData[0] ? (
          React.Children.toArray(
            structuredData.map((widget) => (
              <HomeWidget widget={widget} key={widget.title} />
            ))
          )
        ) : (
          <G_Div
            className="w-full flex items-center justify-center h-[200px] flex-col mt-12 relative"
            onDragEnter={handleDragEnter}
            onDragLeave={handleDragLeave}
            onDragOver={handleDragOver}
            onDrop={handleDrop}
          >
            {isDragging && <G_DropFile />}

            <G_Text pEvents dimension="large" weight="medium" mb={6}>
              {texts?.en?.homePage?.widgets?.left?.but_your_cv}
            </G_Text>

            <G_Text
              className="text-center w-[300px]"
              mb={24}
              dimension="caption"
              pEvents
            >
              {texts.en.homePage.widgets.left.no_cv_desc}
            </G_Text>

            {setFile && (
              <CVInputs
                pEvents={isDragging}
                setFile={setFile}
                loading={isLoading}
              />
            )}
          </G_Div>
        )}
      </G_Div>
    </Col>
  );
};

export default HomeLeftSide;
