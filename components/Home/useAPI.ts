import useAppSelector from '@/hooks/useAppSelector';
import { useChatMutation } from '@/lib/api/endpoints/chatEndpoints';
import { ChatResponse } from '@/lib/types/chat';
import {
  ChatFormValues,
  SingleConversation
} from '@/lib/types/components/homeComponentProps';
import { handleAPIRequests } from '@/utils/handleAPIRequests';
import { FormInstance } from 'antd';
import { useEffect, useRef, useState } from 'react';
import { GPT_DEFAULT_MESSAGE } from '../Auth/consts';
import { ERROR_MESSAGE } from './consts';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns null
 */

export const useSendChatRequest = (form: FormInstance) => {
  const { resume } = useAppSelector((state) => state);

  const focusInput = () => {
    const inputField = form.getFieldInstance('message');
    inputField.focus();
  };

  useEffect(() => {
    focusInput();
  }, [form]);

  const [conversations, setConversations] = useState<SingleConversation[]>([
    {
      id: 0,
      isReplay: true,
      message: GPT_DEFAULT_MESSAGE
    }
  ]);

  // Ask for missing info
  useEffect(() => {
    if (resume) {
      setConversations([
        {
          id: 0,
          isReplay: true,
          message: resume?.data?.missing_info || GPT_DEFAULT_MESSAGE
        }
      ]);
    }
  }, [resume]);

  const messageInputRef = useRef<HTMLInputElement>(null);

  const [chat, { data, isLoading }] = useChatMutation();

  const onFinish = ({ message }: ChatFormValues) => {
    const conversationLastEl = conversations[conversations.length - 1];
    form.resetFields();

    const newRequest = {
      id: conversationLastEl.id + 1,
      isReplay: false,
      message
    };

    const handleSuccess = (res: ChatResponse) => {
      focusInput();

      const newReply = {
        id: conversationLastEl.id + 1,
        isReplay: true,
        message: res?.Assistant
      };

      setConversations([...conversations, newRequest, newReply]);
    };

    const handleFailure = () => {
      focusInput();

      const errorMessage = {
        id: conversationLastEl.id + 1,
        isReplay: true,
        message: ERROR_MESSAGE
      };

      setConversations([...conversations, newRequest, errorMessage]);
    };

    setConversations([...conversations, newRequest]);

    handleAPIRequests({
      request: chat,
      UserChatInput: message,
      handleSuccess: handleSuccess,
      handleFailure: handleFailure
    });
  };

  return { data, isLoading, onFinish, conversations, messageInputRef };
};
