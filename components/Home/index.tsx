import { G_Div } from '../shared';
import G_DashboardContainer from '../shared/Layout/_sub/G_DashboardContainer';
import HomeCenterSide from './CenterSide';
import HomeHeader from './Header';
import HomeLeftSide from './LeftSide';
import HomeRightSide from './RightSide';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const HomePage = (): JSX.Element => {
  return (
    <G_DashboardContainer>
      <HomeHeader />

      <G_Div className="w-[100%] h-full flex items-start gap-6">
        <HomeLeftSide />
        <HomeCenterSide />
        <HomeRightSide />
      </G_Div>
    </G_DashboardContainer>
  );
};

export default HomePage;
