export const dumpWidgets = [
  {
    title: 'Skills',
    list: [
      { icon: './icons/work-alt.svg', label: 'User Research' },
      { icon: './icons/work-alt.svg', label: 'UI/UX' },
      { icon: './icons/work-alt.svg', label: 'Graphic Design' }
    ],
    button: {
      label: 'I want to upskill'
    }
  },

  {
    title: 'Languages',
    list: [
      { icon: './icons/work-alt.svg', label: 'Kinyarwanda' },
      { icon: './icons/work-alt.svg', label: 'English' }
    ],
    button: {
      label: ''
    }
  },
  {
    title: 'Location',
    list: [{ icon: './icons/work-alt.svg', label: 'Kigali, RWANDA' }],
    button: {
      label: 'Explore different locations'
    }
  }
];

export const dumpRightWidgets = [
  {
    title: 'POTENTIAL JOBS',
    list: [
      {
        icon: './icons/star.svg',
        label: 'Product Designer',
        percentage: '70%',
        extendedLabel: ''
      },
      {
        icon: './icons/star.svg',
        label: 'UI/UX Designer',
        percentage: '25%',
        extendedLabel: ''
      },
      {
        icon: './icons/star.svg',
        label: 'UX Designer',
        extendedLabel: '',
        percentage: '85%'
      }
    ],
    button: {
      label: 'Tell me more'
    }
  },

  {
    title: 'POTENTIAL COMPANIES',
    list: [
      {
        icon: './icons/institution.svg',
        label: 'Awesomity Lab',
        extendedLabel: '20 other Companies',
        percentage: ''
      }
    ],
    button: {
      label: 'Connect me'
    }
  }
];

export const ERROR_MESSAGE =
  'Sorry, something is wrong with our Model, we will get back to you soon!';
