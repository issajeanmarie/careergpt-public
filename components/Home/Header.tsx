import { texts } from '@/lib/texts';
import { Col } from 'antd';
import { G_Button, G_Div, G_Input } from '../shared';
import G_UploadCVButton from '../shared/G_Button/_sub/G_UploadCVButton';

/**
 * @since August 2023
 * @author Issa Jean Marie <jeanmarieissa@gmail.com>
 * @see {@link https://issadevs.com} - Author's website
 * @returns {JSX.Element}
 */

const HomeHeader = (): JSX.Element => (
  <G_Div className="flex items-center gap-6 my-6">
    <Col span={7}>
      <G_UploadCVButton
        fluid
        bg="cp_white"
        color="cp_primary"
        iconSrc="/icons/download.svg"
      >
        Download Improved CV
      </G_UploadCVButton>
    </Col>

    <Col flex="auto">
      <G_Input
        className="w-[100%]"
        placeholder={texts?.en?.global?.coming_soon}
        prefixIcon="/icons/linkedin.svg"
        color="cp_white"
        disabled
      />
    </Col>

    <Col>
      <G_Button className="primary-btn-gradient">
        {texts?.en?.global?.buttons?.submit}
      </G_Button>
    </Col>
  </G_Div>
);

export default HomeHeader;
